#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import socket
import socketserver
import sys
import json

def tiempoactual():
    tiempo_actual = time.localtime()
    anio = str(tiempo_actual.tm_year).zfill(2)
    mes = str(tiempo_actual.tm_mon).zfill(2)
    dia = str(tiempo_actual.tm_mday).zfill(2)
    hora = str(tiempo_actual.tm_hour).zfill(2)
    minuto = str(tiempo_actual.tm_min).zfill(2)
    segundo = str(tiempo_actual.tm_sec).zfill(2)

    return f'{anio}{mes}{dia}{hora}{minuto}{segundo}'

try:
    puerto = int(sys.argv[1])
    # tiempo = time
except IndexError:
        print("Usage: python3 serverrtp.py <port>")

class EchoHandler(socketserver.DatagramRequestHandler):
    
    diccion = {} #diccionario inicialmente vacio

    #escriibe el contenido de diccion en registrar.json
    def registrar(self):
        fichero = "registrar.json"
        with open(fichero, "w") as file:
            json.dump([self.diccion], file, indent=3)

    #comprobamos si ya existia un registrar.json y saca lo q ese tiene dentro
    def check_if_registered(self):
        try:
            with open("registrar.json", "r") as jsonfile:
                data = json.load(jsonfile)
            for n in data:
                for key,value in n.items():
                    self.diccion[key] = value
        except IndexError:
            """Si aun no está creado, lo creamos a continuación..."""
            pass

    def handle(self):

        data = self.request[0].decode("utf-8")
        sock = self.request[1]
        method = data.split(" ")[0]
        response = "SIP/2.0 200 OK\r\n\r\n"
        failure = "SIP/2.0 400 Bad Request" "\r\n" + "\r\n"

        self.check_if_registered()

        if method == "REGISTER":
            usuario = data.split(":")[1].split(" ")[0]
            ip = self.client_address[0]
            port = self.client_address[1]
            message = data.split("\r\n")[0]
            print(f"{tiempoactual()} SIP from {self.client_address[0]}:{self.client_address[1]} {message}.")
            direcc = 'sip:' + usuario.split('@')[0] + '@' + str(ip) + ":" + str(port)
            self.diccion[f'sip:{usuario}'] = direcc
            self.registrar()
            sock.sendto(response.encode('utf-8'), self.client_address) # respuesta
            print(f"{tiempoactual()} SIP to {self.client_address[0]}:{self.client_address[1]} {message}.")

        elif method == "INVITE":
            usuario = 'sip:' + data.split(":")[1].split(" ")[0]
            message = data.split("\r\n")[0]
            print(f"{tiempoactual()} SIP from {self.client_address[0]}:{self.client_address[1]} {message}.")
            print(f'ususario: {usuario}')
            print(self.diccion)
            try:
                usuariosip = self.diccion[usuario]
                response = f'SIP/2.0 302 Moved Temporarily\r\nContact: {usuariosip}\r\n\r\n'
            except:
                response = 'SIP/2.0 404 User Not Found\r\n\r\n'

            sock.sendto(response.encode('utf-8'), self.client_address)
            print(f"{tiempoactual()} SIP to {self.client_address[0]}:{self.client_address[1]} {message}.")



def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', puerto), EchoHandler)

        print(f"{tiempoactual()} Starting...")

    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
        socketserver.TCPServer.allow_reuse_address = True
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()

