#!/usr/bin/python3
# -*- coding: utf-8 -*-
import time
import threading
import socketserver
import sys
import simplertp


PORT = 6005  # puerto random q puse (no tiene xq ser el mismo del cliente)
def tiempoactual():
    tiempo_actual = time.localtime()
    anio = str(tiempo_actual.tm_year).zfill(2)
    mes = str(tiempo_actual.tm_mon).zfill(2)
    dia = str(tiempo_actual.tm_mday).zfill(2)
    hora = str(tiempo_actual.tm_hour).zfill(2)
    minuto = str(tiempo_actual.tm_min).zfill(2)
    segundo = str(tiempo_actual.tm_sec).zfill(2)

    return f'{anio}{mes}{dia}{hora}{minuto}{segundo}'
try:
    IPServidorSIP = str(sys.argv[1].split(':')[0])
    puertoServidorSIP = int(sys.argv[1].split(':')[1])
    servicio = sys.argv[2]
    fichero = sys.argv[3]
    tiempo = time
except IndexError:
    print("Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service> <file>")

class EchoHandler(socketserver.DatagramRequestHandler):
    def handle(self):
        data = self.request[0].decode("utf-8")
        sock = self.request[1]
        method = data.split(" ")[0]
        ipservidorRTP = self.client_address[0]
        puertoservidorRTP = self.client_address[1]

        if method == 'INVITE':
            global IPcliente
            global Puertocliente

            IPcliente = data.split('o=')[1].split(' ')[1].split("\r\n")[0]
            Puertocliente = int(data.split('m=')[1].split(' ')[1])
            message = data.split("\r\n")[0]
            # print(message) #esto lo puse para ver si el adicional de la cabecera funcionaba y si
            print(f"{tiempoactual()} SIP from {ipservidorRTP}:{puertoservidorRTP} {message}.")
            response = 'SIP/2.0 200 OK\n'
            sock.sendto(response.encode('utf-8'), self.client_address)
            print(f"{tiempoactual()} SIP to {ipservidorRTP}:{puertoservidorRTP} {response}.")

        if method == 'ACK':
            message = data.split("\r\n")[0]
            print(f"{tiempoactual()} SIP from {ipservidorRTP}:{puertoservidorRTP} {message}.")
            sender = simplertp.RTPSender(ip=IPcliente, port=Puertocliente, file='cancion.mp3', printout=True)
            sender.send_threaded()
            time.sleep(10)
            sender.finish()

        if method == 'BYE':
            response = 'SIP/2.0 200 OK\r\n'
            sock.sendto(response.encode('utf-8'), self.client_address)
            print(f"{tiempoactual()} SIP to {ipservidorRTP}:{puertoservidorRTP} {response}.")
        #Adicional: ERRORES

        elif method != {'INVITE', 'ACK', 'BYE'}:
            response = 'ERROR 405: Method not allowed'
            sock.sendto(response.encode('utf-8'), self.client_address)
            print(f"{tiempoactual()} SIP to {ipservidorRTP}:{puertoservidorRTP} {response}.")

        elif method != type(str):
            response = 'ERROR 400: Bad Request'
            sock.sendto(response.encode('utf-8'), self.client_address)
            print(f"{tiempoactual()} SIP to {ipservidorRTP}:{puertoservidorRTP} {response}.")

def main():
    try:
        serv = socketserver.UDPServer(('Localhost', PORT), EchoHandler)
        print("Listening...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        message = f'REGISTER sip:{servicio}@songs.net SIP/2.0\r\n\r\n'
        serv.socket.sendto(message.encode('utf-8'),(IPServidorSIP,puertoServidorSIP))
    except:
        sys.exit('ERROR registrando Server RTP en Server SIP')
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Fin de conexion")
        sys.exit(0)


if __name__ == '__main__':
    main()
