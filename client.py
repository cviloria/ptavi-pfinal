#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import socket
import socketserver
import time
import threading
import simplertp

IP = "127.0.0.1"
PORT = 34768
def tiempoactual():
    tiempo_actual = time.localtime()
    anio = str(tiempo_actual.tm_year).zfill(2)
    mes = str(tiempo_actual.tm_mon).zfill(2)
    dia = str(tiempo_actual.tm_mday).zfill(2)
    hora = str(tiempo_actual.tm_hour).zfill(2)
    minuto = str(tiempo_actual.tm_min).zfill(2)
    segundo = str(tiempo_actual.tm_sec).zfill(2)

    return f'{anio}{mes}{dia}{hora}{minuto}{segundo}'

try:
    IPServidorSIP = str(sys.argv[1].split(':')[0])
    puertoServidorSIP = int(sys.argv[1].split(':')[1])
    dirCliente = sys.argv[2]
    dirServidorRTP = sys.argv[3]
    tiempo = sys.argv[4]
    fichero = sys.argv[5]

except IndexError:
    print("Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>")


class RTPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        cls.output.close()


def main():
    # Usamos sockets
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

            # enviamos INVITE
            protocol = f'INVITE {dirServidorRTP} SIP/2.0'
            content_type = 'Content-type: application/sdp'
            session_name = dirServidorRTP.split("@")[0].split(":")[1]
            elementos = f'v=0\r\no={dirCliente} {IP}\r\ns={session_name}\r\nt={tiempo}\r\nm=audio {PORT} RTP'
            message = f'{protocol}\r\n{content_type}\r\n\r\n{elementos}'
            my_socket.sendto(message.encode('utf-8'), (IPServidorSIP, puertoServidorSIP))
            print(f' {tiempoactual()} SIP to {IPServidorSIP}:{puertoServidorSIP}: {protocol}')
            # client - servidor
            # vemos la respuesta del servidor y verificamos que es lo que queremos (302)

            repeat = True
            while repeat:
                data, address = my_socket.recvfrom(1024)

                if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 302 Moved Temporarily':
                    print(f'{tiempoactual()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')
                    message = f'ACK {dirServidorRTP} SIP/2.0\r\n\r\n'
                    my_socket.sendto(message.encode('utf-8'), (IPServidorSIP, puertoServidorSIP))
                    print(f'{tiempoactual()} SIP to {IPServidorSIP}:{puertoServidorSIP}: {message}')

                    # una vez comprobado lo anterior
                    # enviamos cliente - servidorRTP el siguiente contenido
                    # asked address = la direccion que nos pide
                    # (Contact en el enunciado de la practica)
                    askedaddress = data.decode('utf-8').split('\r\n')[1].split()[1]
                    askedIPaddress = askedaddress.split('@')[1].split(':')[0]
                    askedPortaddress = int(askedaddress.split('@')[1].split(':')[1])  # lo q queriamos sacar
                    # enviamos INVITE
                    protocol = f'INVITE {askedaddress} SIP/2.0'
                    content_type = 'Content-type: application/sdp'
                    session_name = askedaddress.split("@")[0].split(":")[1]
                    elementos = f'v=0\r\no={dirCliente} {IP}\r\ns={session_name}\r\nt={tiempo}\r\nm=audio {PORT} RTP'
                    #cabecera del cuerpo del paquete
                    header_length = f"Content-Length: {len(elementos.encode('utf-8'))}\r\n"
                    message = f'{protocol}\r\n{content_type}\r\n{header_length}\r\n{elementos}'
                    my_socket.sendto(message.encode('utf-8'), (askedIPaddress, askedPortaddress))
                    print(f'{tiempoactual()} SIP to {askedIPaddress}:{askedPortaddress}: {protocol}')

                    # vemos la respuesta del servidor RTP y comprobamos que es lo que queriamos (200 Ok)

                elif data.decode('utf-8') == 'SIP/2.0 200 OK\n':
                    print(f'{tiempoactual()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')
                    message = f'ACK {dirServidorRTP} SIP/2.0\r\n\r\n'
                    my_socket.sendto(message.encode('utf-8'), (askedIPaddress, askedPortaddress))
                    print(f'{tiempoactual()} SIP to {askedIPaddress}:{askedPortaddress}: {message}')
                    # RTP
                    # Abrimos un fichero para escribir los datos que se reciban
                    RTPHandler.open_output('output.mp3')
                    with socketserver.UDPServer((IP, PORT), RTPHandler) as serv:
                        print("Listening...")
                        # El bucle de recepción (serv_forever) va en un hilo aparte,
                        # para que se continue ejecutando este programa principal,
                        # y podamos interrumpir ese bucle más adelante
                        threading.Thread(target=serv.serve_forever).start()
                        # Paramos un rato. Igualmente podríamos esperar a recibir BYE,
                        # por ejemplo
                        time.sleep(int(tiempo))
                        print("Time passed, shutting down receiver loop.")
                        # Paramos el bucle de recepción, con lo que terminará el thread,
                        # y dejamos de recibir paquetes
                        serv.shutdown()
                    # Cerramos el fichero donde estamos escribiendo los datos recibidos
                    RTPHandler.close_output()

                    mensaje = f'BYE {dirServidorRTP.split("@")[0]}@{address[0]}:{address[1]} SIP/2.0\r\n\r\n'
                    my_socket.sendto(mensaje.encode('utf-8'), (askedIPaddress, askedPortaddress))
                    print(f'{tiempoactual()} SIP to {askedIPaddress}:{askedPortaddress}: {mensaje}')

                    data, address = my_socket.recvfrom(1024)
                    print(f'{tiempoactual()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')
                    if data.decode('utf-8') == 'SIP/2.0 200 OK\r\n\r\n':
                        my_socket.close()

                    repeat = False

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
